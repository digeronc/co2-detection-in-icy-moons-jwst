# -------------------------------------------------
# JWST/NIRSpec IFU/Prism extraction
# Villanueva, Faggi, Cartwright, NASA-GSFC
# Dec 2023
# -------------------------------------------------

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from astropy.io import fits
import scipy.stats
from astropy import units as u
from scipy.signal import find_peaks
from astropy import wcs

# -----------------------------------------------------------------------------------------------------------------
# Useful functions
# -----------------------------------------------------------------------------------------------------------------

def upperlimit(map):
    # ---------------------------------------------------------------------------
    # Calculate the 3 sigma upper limit from a gas abundance map
    # ---------------------------------------------------------------------------
    map[map<0]=0
    med=np.nanmedian(map) # Take the median to avoid taking outliers into account
    std=np.nanstd(map)
    up3s=med+3*std # 3 sigma upper limit
    return up3s

def sigma(map):
    # ---------------------------------------------------------------------------
    # Calculate the standard deviation of a gas abundance map
    # ---------------------------------------------------------------------------
    map[map<0]=0
    std=np.nanstd(map)
    return std

def mmap(map):
    # ---------------------------------------------------------------------------
    # Calculate the mean of a gas abundance map
    # ---------------------------------------------------------------------------
    map[map<0]=0
    m=np.nanmean(map)
    return m

def contour(cx, cy, radius):
    # ---------------------------------------------------------------------------
    # Deline a red circle from a defined center position and radius
    # ---------------------------------------------------------------------------
    # cx: x coordinate of target point
    # cy: y coordinate of target poin
    # radius: radius of the contour
    # ---------------------------------------------------------------------------
    center = (cx, cy)
    radius = radius
    color = 'red'
    circle = plt.Circle(center, radius, color=color, fill=False, linewidth=1)
    return circle

def display_cube(cube, wcs, ox, oy, rad, name, area):
    # ---------------------------------------------------------------------------
    # Display the projected data cube 
    # ---------------------------------------------------------------------------
    # cube: original data cube from FITS file
    # wcs: world coordinates from FITS file
    # ox, oy, rad: reference pixel coordinates (x,y) and radius of the target
    # name, area: name of the target and its specific area
    # ---------------------------------------------------------------------------

    fig = plt.figure()
    ax= fig.add_subplot(projection=wcs, slices=('x','y',500), label='overlays')
   
    circle = contour(ox,oy,rad)

    im1 = ax.imshow(cube, cmap='cividis')

    ax.add_patch(circle)
    ax.plot(ox,oy,'+r')
    ax.coords.grid(True, color='black', ls='solid', alpha=0.2)
    ax.coords[0].set_axislabel('Right Ascension [hh:mm:ss]', fontsize=8)
    ax.coords[1].set_axislabel('Declination [deg]',fontsize=8)

    overlay = ax.get_coords_overlay('galactic')
    overlay.grid(color='black', ls='dotted', alpha=0.5)
    overlay[0].set_axislabel('Galactic Longitude', fontsize=8)
    overlay[1].set_axislabel('Galactic Latitude', fontsize=8)

    plt.title("%s (%s)" % (name,area), fontsize=8, pad=35)
   
    cbar = fig.colorbar(im1, orientation='horizontal',fraction=0.046, pad=0.1)
    cbar.set_label('Surface Brightness [Jy]', fontsize=8)

    plt.show()

# -----------------------------------------------------------------------------------------------------------------
# START
# -----------------------------------------------------------------------------------------------------------------
# Load FITS file from MAST
# TO DO: Change 'base' path to the correct one if needed
# -----------------------------------------------------------------------------------------------------------------

# name = 'Callisto'; area = 'Asgard'; base = 'mast_callisto_level2/JWST/jw02060002001_02101'; vobs=19.338; vsun=-6.492; olon=223.23; olat=2.18; orot=334.73; px=23; py=23.5; odia=4806.00 # 2022/11/25 08:44 UT 223°E 2°N
name = 'Europa'; area = 'Leading'; base = 'mast_europa_level2/jw01250002001_03105'; vobs=10.809; vsun=-13.63; olon=267.03; olat=2.72; orot=334.46; odia=3130.00; px=23; py=23 #2022/11/23 08:16 UT
# name = 'Europa'; area = 'Tara Regio'; base = 'mast_europa_level2/jw01250002001_03105'; vobs=10.809; vsun=-13.63; olon=285; olat=-10; orot=334.46; odia=3130.00; px=23; py=23 #2022/11/23 08:16 UT #odia=1565.00;

# name = 'Callisto'; area = 'Trailing'; base = 'mast_callisto_level2/JWST/jw02060003001_03101'; vobs=30.361; vsun=8.094; olon=81.14; olat=2.22; orot=334.73; px=21.5; py=23.5; odia=4806.00 # 2022/11/15 06:06 UT
# name = 'Callisto'; area = 'Valhalla'; base = 'mast_callisto_level2/JWST/jw02060001001_03101'; vobs=-26.306; vsun=-7.134; olon=296.6; olat=2.85; orot=334.73; px=24.5; py=25.5 # 2023/09/20 02:42 UT

# -----------------------------------------------------------------------------------------------------------------
# Parameters from SPICE
# -----------------------------------------------------------------------------------------------------------------
# vobs: Velocity from observer deldot km/s
# vsun: Velocity from sun km/s
# olon: East longitude (360 - ObsSub-LON) [deg]
# olat: ObsSub-LAT [deg]
# orot: NP.ang [deg]
# odia: Diameter of the object [km]
# -----------------------------------------------------------------------------------------------------------------

# -----------------------------------------------------------------------------------------------------------------
# Constant parameters
# -----------------------------------------------------------------------------------------------------------------
nirang = 138.0 # Angle between NIRCam and NIRSpec
first = True
CS = 299792458.0 # Celerity speed in [m/s]
# Tara Regio of Europa
dtara = 1565.00 # Diameter [km]

# -----------------------------------------------------------------------------------------------------------------
# Variable parameters
# TO DO: Change values if needed
# -----------------------------------------------------------------------------------------------------------------
tara_on = 0  
# -----------------------------------------------------------------------------------------------------------------
# Put tara_on = 1 if you use CO2 gas model from Planetery Spectrum Generator centered on Tara regio with FOV = Europa diameter (3130 km)
# Put tara_on = 0 if FOV=dtara (1565 km)
# -----------------------------------------------------------------------------------------------------------------
if name == 'Callisto':
    ifu = 321.53 # projected area of a pixel [km]
    ndither = 4 # number of dithers
    drad = 8 # radius of the aperture [px]
    dbkg = 4 # radius of the background aperture [px]
    lcorr = 4e-4 # Valhalla = 0.5e-4 # Asgard and Trailing = 4e-4 # redshift correction
elif name == 'Europa':
    ifu =  320.14
    ndither = 2
    drad = 5
    dbkg = 2
    #trad = 2
    #tbkg = 1
    lcorr = 0 
    if (area == 'Tara Regio' and tara_on == 1):
        drad = 3
        dbkg = 2

iref = [500,2500]

# -----------------------------------------------------------------------------------------------------------------
# DATA COLLECTION
# -----------------------------------------------------------------------------------------------------------------
# Recover information from FITS file
# -----------------------------------------------------------------------------------------------------------------
for idet in range(2): # Loop across the detectors (nrs1 and nrs2) which cover different wavelength bands
    for idit in range(ndither): # Loop across the dithers
        if idet==0: sdet='nrs1'
        else: sdet='nrs2'
        file = '%s_%05d_%s_s3d.fits' % (base,idit+1,sdet)
        fr  = fits.open(file)
        hdr = fr["sci"].header
        ang = (fr[0].header['GS_V3_PA'] + nirang)*np.pi/180.0 # v3 position angle of guide star for science 
        ox  = fr[0].header['XOFFSET'] # x offset from pattern starting position 
        oy  = fr[0].header['YOFFSET'] # y offset from pattern starting position 
        dpx = fr[1].header['CDELT1']*3600.0 # coordinate increment converted from degree to second 
        dpy = fr[1].header['CDELT2']*3600.0
        wv0 = fr[1].header['CRVAL3']
        dwv = fr[1].header['CDELT3']
        psr = fr[1].header['PIXAR_SR'] # nominal pixel area in steradian [sr]
        ac2 = fr[1].header['PIXAR_A2'] # nominal pixel area in square arc second [arsec2]
        data = fr["sci"].data*psr*1e6 # from mega jansky per steradian [MJy/sr] to jansky [Jy]
        derr = fr["err"].data*psr*1e6 
        qty = fr["dq"].data
        dnpts = data.shape[0] # number of wavelengths covered (3610)
        xs = data.shape[2] # number of rows (47)
        ys = data.shape[1] # number of columns (49)
        w = wcs.WCS(hdr) # world coordinates
        fr.close()
        
# -----------------------------------------------------------------------------------------------------------------
# DATA CALIBRATION
# -----------------------------------------------------------------------------------------------------------------
       
        #display_cube(data[500,:,:],w,25,25,drad,name,area) # display cube in DEBUG mode

        # Mask low-quality points
        mm = np.nanmedian(derr)
        ind = (qty!=0) + (data>mm*1e5) + (data<-mm*1e3)
        data[ind] = np.nan

        # Declare arrays and create masks
        if first:
            first = False
            npts = dnpts+200
            wvls = np.zeros([2,npts]) #idetector,iband
            noise = np.zeros([2, npts])
            frames = np.zeros([2, ndither, npts, ys, xs]) + np.nan
            ferr = np.zeros([2, ndither, npts, ys, xs]) + np.nan
            maskc = np.zeros([ys,xs]) + np.nan
            maskb = np.zeros([ys,xs]) + np.nan
            tmaskc = np.zeros([ys,xs]) + np.nan
            tmaskb = np.zeros([ys,xs]) + np.nan
            # Create photometric masks
            for k in range(xs):
                for j in range(ys):
                    di = k-px
                    dj = j-py
                    
                    '''if name == 'Europa' and area != 'Tara Regio':
                        ddt =  np.sqrt(di*di + dj*dj)
                        if ddt<=trad: tmaskc[j,k]=1.0
                        elif ddt>trad and ddt<=tbkg+trad: tmaskb[j,k]=1.0'''

                    dd = np.sqrt(di*di + dj*dj)
                    if dd<=drad: maskc[j,k]=1.0
                    elif dd>drad and dd<=dbkg+drad: maskb[j,k]=1.0
                #Endfor
            #Endfor
        #Endif

        # Align frames
        # The dithers are georeferenced to the moon's disk
        dx = -round((ox/dpx)*np.cos(ang) - (oy/dpy)*np.sin(ang))
        dy = -round((ox/dpx)*np.sin(ang) + (oy/dpy)*np.cos(ang))
        if   dx>=0 and dy>=0: 
            frames[idet, idit, 0:dnpts,dy:,dx:] = data[0:dnpts,0:ys-dy,0:xs-dx] # align science data
            ferr[idet, idit, 0:dnpts,dy:,dx:] = derr[0:dnpts,0:ys-dy,0:xs-dx] # align error data
        elif dx>=0 and dy<0:  
            frames[idet, idit, 0:dnpts,0:ys+dy,dx:] = data[0:dnpts,-dy:,0:xs-dx]
            ferr[idet, idit, 0:dnpts,0:ys+dy,dx:] = derr[0:dnpts,-dy:,0:xs-dx]
        elif dx<0  and dy>=0: 
            frames[idet, idit, 0:dnpts,dy:,0:xs+dx] = data[0:dnpts,0:ys-dy,-dx:]
            ferr[idet, idit, 0:dnpts,dy:,0:xs+dx] = derr[0:dnpts,0:ys-dy,-dx:]
        elif dx<0  and dy<0:  
            frames[idet, idit, 0:dnpts,0:ys+dy,0:xs+dx] = data[0:dnpts,-dy:,-dx:]
            ferr[idet, idit, 0:dnpts,0:ys+dy,0:xs+dx] = derr[0:dnpts,-dy:,-dx:]
        
        ind = (np.isfinite(frames[idet, idit, iref[idet], :,:]))
        cmask = np.zeros([ys,xs]) + np.nan
        cmask[ind] = 1.0
        if idet==0 and idit==0: tmask=cmask
        else: tmask=tmask*cmask
        wvls[idet,0:dnpts] = (np.arange(dnpts)*dwv + wv0 + lcorr)*(1.0 - vobs/(CS*1e-3)) # doppler effect
        noise[idet,0:dnpts] = np.nanmedian(np.nanmean(derr,1),1)
    #Endfor
#Endfor

print('CALIBRATION DONE\n')

# -----------------------------------------------------------------------------------------------------------------
# SPECTRAL ANALYSIS
# -----------------------------------------------------------------------------------------------------------------
# Clean frames and perform aperture integration

wvl = np.zeros(npts*2)
spec = np.zeros(npts*2) # data spectrum
k=0
spee = np.zeros(npts*2) # error spectrum
cube = np.zeros([npts*2,ys,xs]) # data cube
cuberr = np.zeros([npts*2,ys,xs]) # error cube
cuberr2 = np.zeros([npts*2,ys,xs]) # squared error cube
npc = np.nansum(tmask*maskc)
npb = np.nansum(tmask*maskb)

for idet in range(2): # Loop across the detectors (nrs1 and nrs2)
    # The dithers are median combined
    dcube = np.nanmedian(frames[idet,:,:,:],0) # data cube
    der = np.nanmedian(ferr[idet,:,:,:],0) # error cube containing standard deviation of each spaxel
    der2 = der**2 # error cube containing variance of each spaxel

    for i in range(npts): # Loop across pixels containing information
        if wvls[idet,i]==0 or noise[idet,i]==0: continue
        img = dcube[i,:,:]*tmask
        e = der[i,:,:]*tmask
        e2 = der2[i,:,:]*tmask
        cnt = np.sum(np.isfinite(img*maskc)) + np.sum(np.isfinite(img*maskb)) # count all the finite numbers (ignore nan and infinite)
        if cnt<(npc+npb-20): continue 

        # Perform aperture integration
        cube[k,:,:] = img
        cuberr[k,:,:] = e
        cuberr2[k,:,:] = e2

        bkg = np.nansum(img*maskb)*(npc/npb) # background
        flx = np.nansum(img*maskc) # flux
        err2 = np.nansum(e2*maskc) # error
        wvl[k] = wvls[idet,i] # wavelength

        # Remove background from flux
        spec[k] = flx - bkg # data spectrum
        spee[k] = np.sqrt(err2) # error spectrum
        #spee[k] = noise[idet,i]*np.sqrt(npc)
        k += 1
    #Endfor
#Endfor

npts = k # number of wavelengths that contain information
wvl = wvl[0:npts]
spec = spec[0:npts] # spectrum
spee = spee[0:npts] # error
cube = cube[0:npts,:,:] # data cube
cuberr = cuberr[0:npts,:,:]
cuberr2 = cuberr2[0:npts,:,:] # error cube

print('CUBE AND SPECTRUM PROCESSED\n')

# ------------------------------------------------------------------------------------------------------------
# Display
# ------------------------------------------------------------------------------------------------------------
# display_cube(cube[1881,:,:],w,px,py,drad,name,area)

'''fig = plt.figure()
ax= fig.add_subplot(projection=w, slices=('x','y',1881), label='overlays')
#fig, axs = plt.subplots(1, 2, subplot_kw={'projection': cube.wcs, 'slices':('x','y',50)})
#circle = contour(ox,oy,drad)
ax.imshow(cube[1881,:,:], cmap='cividis')
ax.imshow(maskc, cmap='inferno')
#ax.add_patch(circle)
ax.plot(px,py,'+r')
#ax.coords.grid(True, color='black', ls='solid', alpha=0.2)
ax.coords[0].set_axislabel('Right Ascension [hh:mm:ss]', fontsize=8)
ax.coords[1].set_axislabel('Declination [deg]',fontsize=8)
overlay = ax.get_coords_overlay('galactic')
overlay.grid(color='black', ls='dotted', alpha=0.5)
overlay[0].set_axislabel('Galactic Longitude', fontsize=8)
overlay[1].set_axislabel('Galactic Latitude', fontsize=8)
plt.title("Masked %s (%s)" % (name,area), fontsize=8, pad=35)
plt.show()'''

print('TEMPERATURE\n')

# ------------------------------------------------------------------------------------------------------------
# TEMPERATURE
# ------------------------------------------------------------------------------------------------------------
# Reflectance maps between 4.25 and 4.27 um

rfl = np.nanmean(cube[1851:1887,:,:],0)
rfl[(np.isfinite(rfl)==0)] = 0.0

# Wavelength ranges
bi1 = np.argmin(abs(wvl-4.3))
bi2 = np.argmin(abs(wvl-4.5)) 
ti1 = np.argmin(abs(wvl-5.0)) 
ti2 = np.argmin(abs(wvl-5.2)) 

# Realistic solar model from PSG
slr = np.genfromtxt('PSG/psg_rad_solar.txt')

# Solar model corrected for the distances between the Sun and the moon, and JWST and the moon
sli = np.interp(wvl*(1.0 - vsun/(CS*1e-3)), slr[:,0], slr[:,1]) # Reflected solar component
scf = np.sum(sli[ti1:ti2])/np.sum(sli[bi1:bi2])

fsc = ifu**2/(np.pi*(odia/2.0)**2.0) # Projected spatial size of the pixel [spaxel area/disk area] 

thb = np.nansum(cube[bi1:bi2:,:,:],0)

# --------------------------------------------------------------------------------------------------------
thr = np.nansum(cube[ti1:ti2:,:,:],0) - thb 
# ---------------------------------------------------------------------------------------------------------------

# Planck source function for the thermal radiation
HP  = 6.626070040e-34  # Planck's constant [W s2] or [J s]
CS  = 299792458.0      # Speed of light [m/s]
KB  = 1.38064852e-23   # Boltzmann' constant [J/K]
R = 8.3145 # [J/mol/K)] Ideal gas constant
S = ifu**2 # Pixel area [m2]

rh = 2 # heliocentric distance in au
vexp = 0.8*rh**-0.5 # Expansion velocity from PSG [km/s]

# ------------------------------------------------------------------------------------------------------------
# Temperature maps (not used)
# ------------------------------------------------------------------------------------------------------------

if name == 'Callisto':
    T = 145 # Estimated surface temperature [K]
    tts = np.arange(130,200,1)
elif name == 'Europa':
    T = 110 # Estimated surface temperature [K]
    tts = np.arange(100,130,1)

sT = 1e6*HP*CS/(KB*T); # 160 was default, 135 seems to work for Valhalla
    
fts = tts*0.0

# ----------------------------------------------------------------------------------------------------------------------
# Estimate object's temperature:
# Fit a two blackbody function to its spectrum in each spaxel
# consisting of a Planck function set to 5777 K (reflected solar component), 
# and a Planck function where temperature is a free parameter (thermal emission component).
# ----------------------------------------------------------------------------------------------------------------------

#temps = [100,110,120,130,140,150,160,170,180,200]
temps = [160]
#ftemps = []

#for temp in temps: #Loop is useless
thf = np.genfromtxt('PSG/psg_rad_%dK.txt' % temps[0]) # [Jy/um] at temp [K]

# Interpolation 
thi = np.interp(wvl, thf[:,0], thf[:,1]) # spectral irradiance [Jy] values associated to each wavelength according to the model

# Planck source function for the thermal radiation
thm = 1.0/ (wvl**3.0 * (np.exp(sT/wvl) - 1.0))
tsc = np.max(thi)/np.max(thm) # ratio between PSG model and Planck source function, it changes at each loop
fts = tts*0.0

# This is the thermal model
for i in range(len(tts)): 
    fts[i] = np.sum(1.0/ (wvl[ti1:ti2]**3.0 * (np.exp((1e6*HP*CS/(KB*tts[i]))/wvl[ti1:ti2]) - 1.0))) *fsc*tsc # total surface flux 

cubea = cube*0.0
cubet = np.zeros([ys,xs])

# Recover the right surface temperature temp for each spaxel
tcount = 0
for x in range(xs):
    for y in range(ys):
        if not np.isfinite(thr[y,x]): continue
        if thr[y,x]>fts[0]: 
            temp = np.interp(thr[y,x], fts, tts)
            tcount +=1
        else: temp = tts[0]
        sT  = 1e6*HP*CS/(KB*temp)
        thf = tsc*fsc / (wvl**3.0 * (np.exp(sT/wvl) - 1.0)) # Thermal component
        
        cubet[y,x] = temp

# ----------------------------------------------------------------------------------------------------------------------
# Reflectance spectra at each spaxel:
# - Remove the thermal component (thf)
# - Divide calibrated fluxes with scaled solar model (sli) - by projected spatial size of the pixel (fsc) - 
# - Correct the distances Sun / moon and JWST / moon (interpolation with wavelengths and velocities)
# ----------------------------------------------------------------------------------------------------------------------

        cubea[:,y,x] = (cube[:,y,x] - thf)/(sli*fsc)

    #Endfor
#Endfor
        
# ----------------------------------------------------------------------------------------------------------------------
# Disk-integrated spectrum:
# Sum the spaxels covering the moon's disk over the 2 dimensions
# ----------------------------------------------------------------------------------------------------------------------

speca = np.nansum(np.nansum(cubea,1),1)*fsc

# ----------------------------------------------------------------------------------------------------------------------
# Save and store spectral files
# ----------------------------------------------------------------------------------------------------------------------

print('Aperture is %.2f arcsec in diameter' % (dpx*(drad*2+1)))
hdu = fits.PrimaryHDU(data=cube)
hdu.writeto('%s_spec.fits' % name, overwrite=True)
hdu = fits.PrimaryHDU(data=cubea)
hdu.writeto('%s_speca.fits' % name, overwrite=True)
hdu = fits.PrimaryHDU(data=cubet)
hdu.writeto('%s_specT.fits' % name, overwrite=True)
fw = open('%s_spec.txt' % name,'w')
fw.write('# Wavelength[um] Flux[Jy] Sigma[Jy] Albedo\n')
for i in range(npts): fw.write('%.6f %e %e %e\n' % (wvl[i],spec[i],spee[i],speca[i]))
fw.close()

# ----------------------------------------------------------------------------------------------------------------------
# SPECTRA
# ----------------------------------------------------------------------------------------------------------------------
# Parameters
# ----------------------------------------------------------------------------------------------------------------------
print('DISPLAY SPECTRA\n')

e =  0.001

l1 = 4.22 # start of wavelength range
l2 = 4.29 # end of wavelength range
i1 = np.argmin(abs(wvl-l1))
i2 = np.argmin(abs(wvl-l2))

w1 = 4.25
w2 = 4.27

wv25 = np.where((wvl >= w1-e) & (wvl <= w1+e))[0]
wv27 = np.where((wvl >= w2-e) & (wvl <= w2+e))[0]

nm = np.nanmean(spec)

# ----------------------------------------------------------------------------------------------------------------------
# Display
# ----------------------------------------------------------------------------------------------------------------------

fig, ax = plt.subplots(2,1,figsize=[10,6])
ax[0].plot(wvl,spec)
ax[0].set_ylabel('Integrated flux [Jy]', fontsize=8)
ax[0].set_title("%s (%s) Integrated Spectrum" % (name,area), fontsize=8)
ax[1].plot(wvl[i1:i2],spec[i1:i2])
ax[1].set_ylabel('Integrated Flux [Jy]', fontsize=8)
ax[1].set_xlabel('Wavelength [um]', fontsize=8)
ax[1].axvline(wvl[i1:i2][np.argmin(spec[i1:i2])], color='r', linestyle='--', linewidth=0.8, label='Wavelength of the minimum value in the CO2 v3 band (%0.3f µm)' % (wvl[i1:i2][np.argmin(spec[i1:i2])]))
ax[1].axvline(np.mean(wvl[wv25]), color='k', linestyle='--', linewidth=0.8, label='Wavelength %0.3f µm' % (np.mean(wvl[wv25])))
ax[1].axvline(np.mean(wvl[wv27]), color='k', linestyle='--', linewidth=0.8, label='Wavelength %0.3f µm' % (np.mean(wvl[wv27])))
#ax[1].set_xlim(wvl[0],wvl[-1])
plt.legend(fontsize=8)
plt.tight_layout()
#plt.savefig('%s_spec.png' % name)
plt.show()
#plt.cla()

print('CO2 GAS ABUNDANCE \n')

# ------------------------------------------------------------------------------------------------------------
# CO2 GAS DETECTION
# ------------------------------------------------------------------------------------------------------------

if name == 'Callisto':

    gas = np.genfromtxt('PSG/psg_rad_IFU_565_100K_callisto_1e19.txt')
    gas_disk = np.genfromtxt('PSG/psg_rad_565_100K_callisto_1e19.txt')
    
elif name == 'Europa':

    if area == 'Tara Regio': # CO2 Gas model centered on Tara Regio at 10°S 285°E
        gas = np.genfromtxt('PSG/psg_rad_IFU_565_100K_tara_1e19.txt')
        if tara_on == 1: # FOV = Estimated diameter of the Tara regio (dtara)
            gas_disk = np.genfromtxt('PSG/psg_rad_565_100K_tara_1e19.txt')
        else: # FOV = Diameter of Europa
            gas_disk = np.genfromtxt('PSG/psg_rad_565_100K_tara_fulldia_1e19.txt')
    else: # Leading hemisphere of Europa
        gas = np.genfromtxt('PSG/psg_rad_IFU_565_100K_europa_1e19.txt')
        gas_disk = np.genfromtxt('PSG/psg_rad_565_100K_europa_1e19.txt')

# ------------------------------------------------------------------------------------------------------------
# CO2 Gas model from Planetary Spectrum Generator
# ------------------------------------------------------------------------------------------------------------
# Pixel scale
# FOV = 0.1"x0.1" (IFU)
# ------------------------------------------------------------------------------------------------------------
nk = 5 # Smooth the sawtooth pattern with a kernel

gp = np.interp(wvl[i1:i2], gas[:,0], gas[:,5]) # Interpolation of co2 gas spectrum from PSG
gc = np.convolve(gp, np.ones(nk)/nk, mode='same') # Generate continuum applying 5-points moving average on co2 gas spectrum

gp = gp[nk:-nk]
gc = gc[nk:-nk]

gd = gp - gc # Generate residuals removing the continuum from the spectrum

# ------------------------------------------------------------------------------------------------------------
# Surface scale
# FOV = specific diameter
# ------------------------------------------------------------------------------------------------------------
gp_disk = np.interp(wvl[i1:i2], gas_disk[:,0], gas_disk[:,5])
gc_disk = np.convolve(gp_disk, np.ones(nk)/nk, mode='same')

gp_disk = gp_disk[nk:-nk]
gc_disk = gc_disk[nk:-nk]

gd_disk = gp_disk - gc_disk

print('GAS MODEL DONE\n')

# ------------------------------------------------------------------------------------------------------------
# Plot CO2 models
# ------------------------------------------------------------------------------------------------------------

'''plt.figure()
plt.subplot(211)
plt.plot(wvl[i1+nk:i2-nk],gd_disk, color='tab:orange', label='FOV = %0.0f km' % (odia))
plt.xlabel('Wavelength [um]', fontsize=8)
plt.ylabel('Spectral irradiance [Jy]', fontsize=8)
plt.title('CO2 Gas Model (%s %s)' % (name, area), fontsize=8)
plt.legend(fontsize=8)
plt.subplot(212)
plt.plot(wvl[i1+nk:i2-nk],gd, color='black', label='FOV = 0.1\"x0.1\"')
plt.xlabel('Wavelength [um]', fontsize=8)
plt.ylabel('Spectral irradiance [Jy]', fontsize=8)
plt.legend(fontsize=8)
plt.show()

print('pause')'''

# ------------------------------------------------------------------------------------------------------------
# Gas map and residual spectra
# ------------------------------------------------------------------------------------------------------------
print('GAS MAP AND RESIDUALS\n')

st = gd*0.0
gt = gd*0.0
ct = gd*0.0
tt = wvl*0.0
var = gd*0.0

# Initialize CO2 gas abundance map in unit of column density defined in PSG: 1e19 m-2

gasmap = np.zeros([ys,xs])

# ------------------------------------------------------------------------------------------------------------
# Pixel-by-pixel processing
# ------------------------------------------------------------------------------------------------------------

for x in range(xs):
    for y in range(ys): # Loop across each spaxel
        if not np.isfinite(maskc[y,x]) or not np.isfinite(cube[i1,y,x]): continue # While we are not within the extraction mask
       
        sp = cube[i1:i2,y,x] # reflectance spectrum
        esp2 = cuberr2[i1:i2,y,x] # squared error propagation spectrum
        tt+= cube[:,y,x] # disk integrated spectrum

        if (x==23 or x==24) and y==25: # Manual correction of bad pixel
            sp[76]=sp[75]
            sp[77]=sp[78]

        sc = np.convolve(sp, np.ones(nk)/nk, mode='same') # Generate local continuum for each spaxel
       
        sp = sp[nk:-nk]
        sc = sc[nk:-nk]

        esp2 = esp2[nk:-nk]

        # Residual spectrum
        sd = sp - sc 

        ct+= sc # Disk integrated continuum
        st+= sd # Disk integrated residual
        var+= esp2 # Disk integrated error

        # Compute normalized cross-correlation factor for gas map
        gf = np.sum(sd*gd)/np.sum(gd*gd) # [-1,1]

        # Scale CO2 model with cross-correlation factor       
        gt+= gd*gf

        # CO2 gas abundance map
        gasmap[y,x] = gf

    #Endfor
#Endfor

# Build the 1 sigma error propagation
stderr = np.sqrt(var)
stdm = np.nanmean(stderr)

# ------------------------------------------------------------------------------------------------------------
# Surface scale model
# ------------------------------------------------------------------------------------------------------------

tts = tt[i1+nk:i2-nk] # Disk integrated spectrum
tsp = tts - ct # Residual

tgf= np.sum(tsp*gd_disk)/np.sum(gd_disk*gd_disk) # Normalized cross-correlation factor with CO2 model FOV=Disk
tsp_m = np.nanmean(tsp)

print('ERROR PROPAGATION\n')

# ------------------------------------------------------------------------------------------------------------
# ERROR ANALYSIS
# ------------------------------------------------------------------------------------------------------------
err1s = np.nanstd(tsp) # 1 sigma error of residuals
err3s = 3*err1s # 3 sigma error of residuals

gf_3s = 3*sigma(gasmap*maskc) # 3 sigma upper limit of CO2 gas abundance map
stderr_3s = 3*stderr # 3 sigma propagated error from FITs file ERR array


# ------------------------------------------------------------------------------------------------------------
# Scaled CO2 model   
# ------------------------------------------------------------------------------------------------------------

tgt = gd_disk*tgf
gt_3s = tgt/gf_3s # CO2 model associated to 3 sigma error

# ------------------------------------------------------------------------------------------------------------
# Display 1 sigma error propagation
# ------------------------------------------------------------------------------------------------------------
plt.figure()

plt.subplot(211)

plt.plot(wvl[i1+nk:i2-nk], tsp, color='black', linewidth=1, label='Residual spectrum')
#plt.plot(wvl[i1+nk:i2-nk],tgt, color='tab:orange', label=r"CO2 gas abundance = %0.6fe19 m-2" % (tgf))
plt.fill_between(wvl[i1+nk:i2-nk], tsp-stderr, tsp+stderr, alpha=0.2, color='purple', label=r"Disk integrated 1$\sigma$ error propagation")
plt.xlabel('Wavelength [um]', fontsize=8)
plt.ylabel('Spectral irradiance [Jy]', fontsize=8)
plt.legend(fontsize=8, loc='lower left')
plt.title('Residual spectrum of %s (%s) in the CO2 v3 band [4.2, 4.3] um' % (name,area), fontsize=8)

plt.subplot(212)

plt.plot(wvl[i1+nk:i2-nk], tsp, color='black', linewidth=1, label='Residual spectrum')
plt.plot(wvl[i1+nk:i2-nk],tgt, color='tab:orange', label=r"CO2 gas abundance = %0.6fe19 m-2" % (tgf))
plt.fill_between(wvl[i1+nk:i2-nk], tsp_m-err1s, tsp_m+err1s, alpha=0.2, color='green', label=r"1$\sigma$ limits, |$\sigma$|= %0.6f"  % (err1s))
plt.xlabel('Wavelength [um]', fontsize=8)
plt.ylabel('Spectral irradiance [Jy]', fontsize=8)
plt.legend(fontsize=8, loc='lower left')
#plt.title('Residual spectrum of %s (%s) spanning [4.2, 4.3] um' % (name,area), fontsize=8)

plt.show()

print('pause')

# ------------------------------------------------------------------------------------------------------------
# Display 3 sigma error propagation
# ------------------------------------------------------------------------------------------------------------
# Surface scale
# ------------------------------------------------------------------------------------------------------------
plt.figure()
plt.subplot(211)
plt.plot(wvl[i1+nk:i2-nk],tsp, color='black', linewidth=0.8, label='Residual of Disk Integrated Spectrum')
plt.fill_between(wvl[i1+nk:i2-nk],tsp-stderr_3s,tsp+stderr_3s, alpha=0.2, label=r"Disk integrated 3$\sigma$ error propagation")
plt.xlabel('Wavelength [um]', fontsize=8)
plt.ylabel('Spectral irradiance [Jy]', fontsize=8)
plt.legend(fontsize=7, loc='lower left')
plt.title('Residual spectrum of %s (%s) spanning [4.2, 4.3] um' % (name,area), fontsize=8)
plt.subplot(212)
plt.plot(wvl[i1+nk:i2-nk],tsp, color='black', linewidth=0.8, label='Residual of Disk Integrated Spectrum')
plt.plot(wvl[i1+nk:i2-nk],gt_3s, color='tab:orange', label=r"3$\sigma$ CO2 gas abundance = %0.6fe19 m-2" % (gf_3s))
plt.fill_between(wvl[i1+nk:i2-nk],tsp_m-err3s,tsp_m+err3s, alpha=0.2, color='green', label=r"3$\sigma$ limits, |3$\sigma$| = %0.6f" % (err3s))
plt.xlabel('Wavelength [um]', fontsize=8)
plt.ylabel('Spectral irradiance [Jy]', fontsize=8)
plt.legend(fontsize = 7,loc='lower left')
#plt.savefig('%s_gas_disk.png' % name)
plt.show()

print('pause')

# ------------------------------------------------------------------------------------------------------------
# Store residual data in CSV file for each moon to superpose them
# TO DO: Change file path 
# ------------------------------------------------------------------------------------------------------------

# Path to the CSV file
'''if name == 'Europa':
    csv_file_path_europa = 'PSG/residual_europa.npy'
    np.save(csv_file_path_europa, st)
if name == 'Asgard':
    csv_file_path_callisto = 'PSG/residual_asgard.npy'
    np.save(csv_file_path_callisto, st)

# Load the saved array

load_residual_asgard = np.load('PSG/residual_asgard.npy')
load_residual_europa = np.load('PSG/residual_europa.npy')'''
#print(load_residual_asgard)

# ------------------------------------------------------------------------------------------------------------
# Find peaks in signals
# TO DO: Change H (treshold) if needed
# ------------------------------------------------------------------------------------------------------------
if name == 'Callisto':
    H = 0.002
elif name == 'Europa':
    H = 5e-6

peaks, _ = find_peaks(gt, height=H)
peaks_obs, _ = find_peaks(st, height=H)

# ------------------------------------------------------------------------------------------------------------
# Display Disk integrated spectrum and residuals
# ------------------------------------------------------------------------------------------------------------
# Pixel scale
# ------------------------------------------------------------------------------------------------------------

fig, ax = plt.subplots(2,1,figsize=[12,8])

ax[0].plot(wvl[i1+nk:i2-nk],ct,color='red', label='Continuum')
ax[0].plot(wvl[i1-20:i2+20],tt[i1-20:i2+20],color='black',linewidth=0.8, label='Disk Integrated Spectrum')
ax[0].set_ylabel('Integrated flux [Jy]', fontsize=8)
ax[0].set_title('Disk Integrated spectrum of %s (%s) spanning [4.2, 4.3] um' % (name,area), fontsize=8)
ax[0].legend(fontsize=8, loc='lower left')

ax[1].plot(wvl[i1+nk:i2-nk],st,color='black',linewidth=0.5, label='Residual Spectrum')
ax[1].fill_between(wvl[i1+nk:i2-nk],st-stderr,st+stderr, alpha=0.2, label=r"Disk integrated 1$\sigma$ error propagation")
ax[1].plot(wvl[i1+nk:i2-nk],gt,color='tab:orange', linewidth=1, label=r"CO2 gas abundance = %0.6fe19 m-2" % (np.nanmedian(gasmap*maskc)))

#ax[1].plot(wvl[i1+nk+peaks],gt[peaks], 'x', color='tab:orange') # Display peaks markers if needed
#ax[1].plot(wvl[i1+nk+peaks],st[peaks],'ko')

ax[1].set_ylabel('Residual flux [Jy]', fontsize=8)
ax[1].set_xlabel('Wavelength [um]', fontsize=8)
ax[1].legend(fontsize=8, loc='lower left')
ax[1].set_xlim(l1,l2)
ax[1].set_title('Residual spectrum of %s (%s) spanning [4.2, 4.3] um' % (name,area), fontsize=8)
#plt.savefig('%s_gas_ifu.png' % area)

plt.show()

'''
fig, ax = plt.subplots(2,1)
ax[0].plot(wvl[i1+nk:i2-nk],ct,color='red', label='Continuum')
ax[0].plot(wvl[i1-20:i2+20],tt[i1-20:i2+20],color='black',linewidth=0.8, label='Disk Integrated Spectrum')
ax[0].set_ylabel('Integrated flux [Jy]', fontsize=8)
ax[0].set_title('Disk Integrated spectrum of %s (%s) spanning [4.2, 4.3] um' % (name,area), fontsize=8)
ax[0].legend(fontsize=8, loc='lower left')
ax[1].plot(wvl[i1+nk:i2-nk],st,color='black',linewidth=0.5, label='Residual Spectrum')
ax[1].plot(wvl[i1+nk:i2-nk],gt,color='tab:orange', label="Disk integrated CO2 gas model at IFU scale")
#ax[1].plot(wvl[i1+nk+peaks],gt[peaks], 'x', color='tab:orange')
#ax[1].plot(wvl[i1+nk+peaks],st[peaks],'ko')
ax[1].set_ylabel('Residual flux [Jy]', fontsize=8)
ax[1].set_xlabel('Wavelength [um]', fontsize=8)
ax[1].legend(fontsize=8, loc='lower left')
ax[1].set_xlim(l1,l2)
ax[1].set_title('Residual spectrum of %s (%s) spanning [4.2, 4.3] um' % (name,area), fontsize=8)
#plt.savefig('%s_gas_ifu.png' % area)
plt.show()'''

# ------------------------------------------------------------------------------------------------------------
# Display superposition of Europa and Callisto residuals
# ------------------------------------------------------------------------------------------------------------
'''plt.figure()
plt.plot(wvl[i1+nk-1:i2-nk],load_residual_asgard,color='purple',linewidth=0.8, label='Asgard Residual Spectrum')
plt.plot(wvl[i1+nk:i2-nk],load_residual_europa,color='black', linewidth=0.8, label='Europa Résidual Spectrum')
plt.ylabel('Residual flux [Jy]')
plt.xlabel('Wavelength [um]')
plt.legend(fontsize='8', loc='lower left')
plt.xlim(l1,l2)
plt.title('Residual spectra of Europa and Callisto [4.22 4.29] um', fontsize=8)
#plt.savefig('%s_gas.pdf' % name)
#plt.cla()
plt.show()'''

'''plt.figure()
plt.plot(gt)
plt.plot(peaks, gt[peaks], "x")
plt.show()'''

print('MAPS\n')

# ------------------------------------------------------------------------------------------------------------
# MAPS 4.25 AND 4.27 um
# ------------------------------------------------------------------------------------------------------------
# Useful functions
# ------------------------------------------------------------------------------------------------------------

def globe(panel, r):
    rad=r; pk=np.pi/180.0
    pr = np.arange(360)*pk
    rx = rad*np.cos(pr); ry = rad*np.sin(pr)
    ax[panel].plot(rx,ry,color='white')
    for lon in np.arange(0,375,15): # orthographic projection
        lat = np.arange(-90,95,5)
        rx = rad*np.cos(lat*pk)*np.sin((lon-olon)*pk)
        ry = rad*(np.cos(olat*pk)*np.sin(lat*pk) - np.sin(olat*pk)*np.cos(lat*pk)*np.cos((lon-olon)*pk))
        otn = np.sin(lat*pk)*np.sin(olat*pk) + np.cos(lat*pk)*np.cos(olat*pk)*np.cos((olon-lon)*pk)
        ind = (otn>0.0).nonzero()
        rx[ind] = np.nan
        ax[panel].plot(rx,ry,':',color='white')
    #Endfor
    for lat in np.arange(-90,120,30):
        lon = np.arange(0,360,5)
        rx = rad*np.cos(lat*pk)*np.sin((lon-olon)*pk) # https://en.wikipedia.org/wiki/Orthographic_map_projection
        ry = rad*(np.cos(olat*pk)*np.sin(lat*pk) - np.sin(olat*pk)*np.cos(lat*pk)*np.cos((lon-olon)*pk))
        otn = np.sin(lat*pk)*np.sin(olat*pk) + np.cos(lat*pk)*np.cos(olat*pk)*np.cos((olon-lon)*pk)
        ind = (otn>0.0).nonzero()
        rx[ind] = np.nan
        ax[panel].plot(rx,ry,':',color='white')
    #Endfor
#Enddef

# Rotate arrays
def rotimg(img, px, py, angle):
    alias = 10
    rxs = img.shape[1]; rys = img.shape[0]
    bimg = np.repeat(np.repeat(img, alias, axis=0), alias, axis=1)
    rimg = bimg*0.0
    cosa = np.cos(angle*np.pi/180.0)
    sina = np.sin(angle*np.pi/180.0)
    for x in range(rxs*alias):
        dx = (x/alias-px)
        for y in range(rys*alias):
            dy = (y/alias-py)
            rx = int(np.round(alias*(dx*cosa - dy*sina + px)))
            ry = int(np.round(alias*(dx*sina + dy*cosa + py)))
            if rx<0 or rx>=rxs*alias or ry<0 or ry>=rys*alias: continue
            rimg[y,x] = bimg[ry,rx]
        #Endfor
    #Endfor
    return rimg
#Endef

# ------------------------------------------------------------------------------------------------------------
# Display maps
# ------------------------------------------------------------------------------------------------------------
rlim = 1.4
extent = np.asarray([0-px-0.5,xs-px-0.5,ys-py-0.5,0-py-0.5])*ifu/(0.5*odia)

if name == 'Callisto':
    fig, ax = plt.subplots(1,2,figsize=[18,4])
    ax[0].set_title('Mean reflected emission around 4.25 um [mJy]', fontsize=8)
    rimg = rotimg(rfl, px, py, orot)*1e3
    p0 = ax[0].imshow(rimg, extent=extent,cmap='inferno')
    plt.colorbar(p0,ax=ax[0])
    ax[0].set_ylim(-rlim,rlim)
    ax[0].set_xlim(-rlim,rlim)
    globe(0,1)
    ax[1].set_title('CO2 gas density (%s, %s) [1E19 m-2]' % (name,area), fontsize=8)
    rimg = rotimg(gasmap*maskc, px, py, orot)
    p1 = ax[1].imshow(rimg,extent=extent,vmin=0)
    plt.colorbar(p1,ax=ax[1])
    ax[1].set_ylim(-rlim,rlim)
    ax[1].set_xlim(-rlim,rlim)
    globe(1,1)

elif name == 'Europa':
    fig, ax = plt.subplots(1,2,figsize=[18,4])
    ax[0].set_title('Mean reflected emission around 4.25 um [mJy]', fontsize=8)
    rimg = rotimg(rfl, px, py, orot)*1e3
    p0 = ax[0].imshow(rimg, extent=extent,cmap='inferno')
    plt.colorbar(p0,ax=ax[0])
    ax[0].set_ylim(-rlim,rlim)
    ax[0].set_xlim(-rlim,rlim)
    globe(0,1)

    ax[1].set_title('CO2 gas density (%s, %s) [1E19 m-2]' % (name,area), fontsize=8)
    rimg = rotimg(gasmap*maskc, px, py, orot)
    p1 = ax[1].imshow(rimg,extent=extent,vmin=0, vmax=0.015)
    plt.colorbar(p1,ax=ax[1])
    ax[1].set_ylim(-rlim,rlim)
    ax[1].set_xlim(-rlim,rlim)
    globe(1,1)

    '''p2 = ax[2].imshow(rfl_25*0.0,extent=extent,vmin=0, vmax=1,cmap='magma')
    ax[2].set_title('Globe simulation')
    ax[2].set_ylim(-rlim,rlim)
    ax[2].set_xlim(-rlim,rlim)
    globe(2,1)'''

    '''ax[2].set_title('Surface temperature [K]', fontsize=8)
    rimg = rotimg(cubet, px, py, orot)
    p2 = ax[2].imshow(rimg,extent=extent,vmin=np.nanmin(cubet),vmax=np.max(cubet),cmap='turbo')
    plt.colorbar(p2,ax=ax[2])
    ax[2].set_xlim(-rlim,rlim)
    globe(2,1)'''

plt.tight_layout()
#plt.savefig('%s_maps.png' % area)
plt.show()

# ------------------------------------------------------------------------------------------------------------
# STATISTICS
# ------------------------------------------------------------------------------------------------------------
print('STATISTICS\n')

# Pearson coefficient
pearsoncoef = scipy.stats.pearsonr(st, gt) # Pearson coefficient to assess the relevance of the model
pearsoncoef_disk = scipy.stats.pearsonr(tsp, tgt)

# Upper limits 1 sigma and 3 sigma
gas_3s_up = upperlimit(rimg) # 3 sigma upper limit of CO2 gas abundance map
gas_3s = 3*sigma(rimg)
gas_mean = mmap(rimg)
gas_1s = sigma(rimg)

rimg[rimg<0]=0

M = np.nanmax(rimg)
m = np.nanmin(rimg)
med = np.nanmedian(rimg)

if name == 'Callisto':
    print('Pearson coefficient IFU = %.6f\nPearson coefficient Disk = %.6f\n1s upper limit CO2 gas abundance = %.6fe19 m-2\n1s CO2 gas abundance = %.6fe19 m-2\n3s upper limit CO2 gas abundance = %.6fe19 m-2\n3s CO2 gas abundance = %.6fe19 m-2\nMean CO2 gas abundance (FOV=IFU) = %.6fe19m-2\nCO2 gas abundance (FOV=Disk) = %0.6fe19m-2\nMedian CO2 gas abundance (FOV=IFU) = %0.6fe19m-2' % (pearsoncoef.statistic, pearsoncoef_disk.statistic, gas_1s+gas_mean, gas_1s, gas_3s_up, gas_3s, gas_mean, tgf, med))
if name == 'Europa':
    print('Pearson coefficient IFU = %.6f\nPearson coefficient Disk = %.6f\n1s upper limit CO2 gas abundance = %.6fe19 m-2\n1s CO2 gas abundance = %.6fe19 m-2\n3s upper limit CO2 gas abundance = %.6fe19 m-2\n3s CO2 gas abundance = %.6fe19 m-2\nMean CO2 gas abundance (FOV=IFU) = %.6fe19m-2\nCO2 gas abundance (FOV=Disk) = %0.6fe19m-2\nMedian CO2 gas abundance (FOV=IFU) = %0.6fe19m-2' % (pearsoncoef.statistic, pearsoncoef_disk.statistic, gas_1s+gas_mean, gas_1s, gas_3s_up, gas_3s, gas_mean, tgf, med))

print('END')

# -----------------------------------------------------------------------------------------------------------------
# STOP
# -----------------------------------------------------------------------------------------------------------------

# Additional plots

'''plt.figure()
#plt.subplot(121)
plt.imshow(cube[1000,:,:], cmap='cividis')
plt.imshow(maskc, cmap='inferno')
plt.title('%s %s - JWST NIRSpec' % (name,area), fontsize=8)
plt.show()

print('pause')

plt.figure()
#plt.subplot(122)
plt.imshow(maskc, cmap='cividis')
plt.title('%s %s - Disk Extraction Mask' % (name, area), fontsize=8)
plt.show()

print('pause')'''

'''fig = plt.figure()
ax= fig.add_subplot(projection=w, slices=('x','y',500), label='overlays')
#fig, axs = plt.subplots(1, 2, subplot_kw={'projection': cube.wcs, 'slices':('x','y',50)})
#circle = contour(ox,oy,drad)
ax.imshow(cube[1000,:,:], cmap='cividis')
ax.imshow(maskc, cmap='inferno')
#ax.add_patch(circle)
ax.plot(px,py,'+r')
#ax.coords.grid(True, color='black', ls='solid', alpha=0.2)
ax.coords[0].set_axislabel('Right Ascension [hh:mm:ss]', fontsize=8)
ax.coords[1].set_axislabel('Declination [deg]',fontsize=8)
overlay = ax.get_coords_overlay('galactic')
overlay.grid(color='black', ls='dotted', alpha=0.5)
overlay[0].set_axislabel('Galactic Longitude', fontsize=8)
overlay[1].set_axislabel('Galactic Latitude', fontsize=8)

plt.title("Masked %s (%s)" % (name,area), fontsize=8, pad=35)
plt.show()

print('end')'''
   